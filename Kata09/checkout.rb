require 'test/unit'

class CheckOut

  def initialize
    @total = 0
    @items = Array.new
    @rules = { "A" => { :unity => 50, :amount => 3, :discount => 20 },
    "B" => { :unity => 30, :amount => 2, :discount => 15 },
    "C" => { :unity => 20, :amount => 0, :discount => 0 },
    "D" => { :unity => 15, :amount => 0, :discount => 0 } }
  end

  def scan(item)
    if @rules.key? item
      @items.push item

      itemRule = @rules[item]
      calc_total item, itemRule[:unity], itemRule[:amount], itemRule[:discount]

    end
  end

  def calc_total(item, price, amount_for_discount, discount)
    if item == "A" || item == "B"
      apply_discount = (@items.count(item) % amount_for_discount) == 0
    else
      apply_discount = false
    end
    #puts "Devo aplicar disconto ? #{apply_discount}"
    if apply_discount
      @total += price
      @total -= discount
      #puts "Total atual: #{@total}"
    else
      #puts "Calculando valor normal: ++#{price}"
      @total += price
    end
  end

  def total
    @total
  end

  def print_items
    puts @items
  end

end

class TestPrice < Test::Unit::TestCase

  def test_incremental
    co = CheckOut.new
    assert_equal(  0, co.total)
    co.scan("A");  assert_equal( 50, co.total)
    co.scan("B");  assert_equal( 80, co.total)
    co.scan("A");  assert_equal(130, co.total)
    co.scan("A");  assert_equal(160, co.total)
    co.scan("B");  assert_equal(175, co.total)
  end

  def price(goods)
    co = CheckOut.new
    goods.split(//).each { |item| co.scan(item) }
    co.total
  end

  def test_totals
    assert_equal(  0, price(""))
    assert_equal( 50, price("A"))
    assert_equal( 80, price("AB"))
    assert_equal(115, price("CDBA"))

    assert_equal(100, price("AA"))
    assert_equal(130, price("AAA"))
    assert_equal(180, price("AAAA"))
    assert_equal(230, price("AAAAA"))
    assert_equal(260, price("AAAAAA"))

    assert_equal(160, price("AAAB"))
    assert_equal(175, price("AAABB"))
    assert_equal(190, price("AAABBD"))
    assert_equal(190, price("DABABA"))
  end

end
